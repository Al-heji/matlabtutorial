%% Chaptar30a
% Scalar: Any individual real or complex number is repreaented in  ...
...MATLAB AS A 1-by-1 matrix called a scalar value:

A = 14;
ndims (A) % Check number of dimensions in A 
size(A) % Check value of row and column dimensions
isscalar(A)
 %% vector 
 A  = [5.73 2-4i 9/7 25e3 .046 sqrt(32) 8j];
 size(A) % check value of row and column dimensions 
 
 %% vector 2
 A = [29 43 77 9 21 ];
 B = [0 46 11];
 C = [A 5 ones(1,3) B];
 
%%
 A = [5.36; 7.01; []; 9.44]
 isvector(A) % use the isvector function to tell if a variable holds a vector 1 means...
 ... yes 0 means No
 
%% Examples 
% define a row vector with components the numbers 1,2,3,4,5 assigning it a
%
% variable name x
x = [1 2 3 4 5 ];
% create a column vector with components th enumbers 6,7,8,9,10 and
% assigning it a variable name y
y = [6;7;8;9;10];
%or you cant transpose the raw vector y 
y = [6,7,8,9,10];
y';

%% vectors
%Define a vector and assign avariable name to it by specifying the first
%entry an increment and the last entry 
u = [0:8]
%obtain a vector whose entries are 0,2,4,6, and 8 
v = [0:2:8]

v(1:3)
% %% Matrix
 A = [1 2 3; 3 4 5; 6 7 8];
% %you can refer to  a particular entry in  a matrix by using  parentheses 
 A(2,3);
% %extract submatrices using a similar notation as above. for example to
% %obtain the submatrix that consists of the first two rows and last two
% %columns of A we type 
 A(1:2,2:3)

%% matrix 
B = [-1 3 10; -9 5 25; 0 14 2]
s = [-1 8 5 ]
t = [7;0;11]

% to subtract 1 form every entry entry in the matrix A we type
A-1
%add (or subtract) two compatible matrices (i.e matrices of the same size);
 A+B
 %s-t
 %???Error using ===>- ...
 ...Matrix dimensions must agree
 %this error was expected since s has size 1-by3 and t has size 
s-t'
B*t
%% MatricDivision
%if M is an invertible square matrix and b is a compatible vector then 
%x = M\b is the solution of M x = b and ...
... x = b/M is the solution of x 


%%
%s*s ??? Error using 
s.*s
s.^2
%%polynomials
%Even though MatLAB is a numerical package, it has capabilities for
%handling polynomials.
%in Matlab, a  ploynomial is represented by a vector containing its
%coefficients in descending order.
%fof instanc, the following polynomial 
%p(x) = x^2 - 3x + 5 
%is represented by the vector
p = [1, -3, 5]
q = [1, 0, 7,-1,0]
% and the polynomial p (x) = x^2
 polyval(q,-1) % you can find the value of  a poly
 roots(q)
  % command conv 
  s = [1 2];
  t = [1 4 8];
  z = conv(s,t)
  
  % command deconv 
  [s,r] = deconv(z,t)
   
%roots(p)
%
%
%% help 
help deconv

 